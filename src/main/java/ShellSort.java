import java.util.Arrays;

public class ShellSort {


    private int swapCounter;
    private int stepChangeCounter;

    public void setSwapСounter(int swapCounter) {
        this.swapCounter = swapCounter;
    }

    public void setStepChangeCounter(int stepChangeCounter) {
        this.stepChangeCounter = stepChangeCounter;
    }

    public int getSwapCounter() {
        return swapCounter;
    }

    public int getStepChangeCounter() {
        return stepChangeCounter;
    }


    public int[] shellSort(int[] list){
        this.setSwapСounter(0);
        this.setStepChangeCounter(0);
        int numberOfElements = list.length;
        for (int step = numberOfElements / 2; step > 0; step /= 2) {
            this.stepChangeCounter++;
            for (int i = step; i < numberOfElements; i++) {
                for (int j = i - step; j >= 0 && list[j] > list[j + step] ; j -= step) {
                    this.swapCounter++;
                    int temporaryElement= list[j];
                    list[j] = list[j + step];
                    list[j + step] = temporaryElement;
                }
            }
        }
        return list;
    }


}
