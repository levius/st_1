package Model;


import java.util.*;

public class Space {
    Collection<Star> stars = new LinkedList<Star>();
    Collection<SoundSource> soundSources = new LinkedList<SoundSource>();

    public Space() {
    }

    public Star getClosestStar(Location location){
        int minDistance = Integer.MAX_VALUE;
        Star closestStar = null;

        for (Star star: stars) {
            if (star.getLocation().getDistance(location) < minDistance)
                closestStar = star;
                minDistance = star.getLocation().getDistance(location);
        }
        return closestStar;
    }

    public SoundSource getClosestSoundSource(Location location){
        int minDistance = Integer.MAX_VALUE;
        SoundSource closestSoundSource = null;
        for (SoundSource soundSource: soundSources) {
            if (soundSource.getLocation().getDistance(location) < minDistance)
                closestSoundSource = soundSource;
                minDistance = soundSource.getLocation().getDistance(location);
        }
        return closestSoundSource;
    }

    public int getNoiseLevel(Location location){
        if (getClosestSoundSource(location).getLocation().getDistance(location) > 100){
            //noise is not spreading in space
            return 0;
        } else {
            //but actually we can hear the engine
            int loudestSound = 0;
            for (SoundSource soundSource: soundSources) {
                if( (soundSource.getNoiseLevel() > loudestSound) && (soundSource instanceof Engine) && (( (Engine) soundSource).isWorking()))
                    loudestSound = soundSource.getNoiseLevel();
            }
            return loudestSound;
        }
    }

    public double getLightLevel(Location location){
        //although light can travel in space
        double shiningLevel = 0;
        for (Star star: stars) {
            int distance = star.getLocation().getDistance(location);
            if (distance<1) distance = 1;
            shiningLevel += star.getShiningLevel()/distance;
        }
        return shiningLevel;
    }

    public void addStar(Star star){
        stars.add(star);
    }

    public void addSoundSource(SoundSource soundSource){
        soundSources.add(soundSource);
    }


    public void humanWentFlying(Human human){
        SoundSource engine = getClosestSoundSource(human.getLocation());
        System.out.println(engine.getLocation().getDistance(human.getLocation()));
        if ((engine instanceof Engine) && (( (Engine) engine).isWorking()) && engine.getNoiseLevel() > 200 && engine.getLocation().getDistance(human.getLocation()) < 20){
            human.setFlying(true);
        } else {
            human.setFlying(false);
        }
    }
}