import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExсeptionTest {

    @Test
    public void testLess() {
        Assertions.assertThrows(IllegalArgumentException.class,() -> Calculation.acos(-1.00001));
    }

    @Test
    public void testMore() {
        Assertions.assertThrows(IllegalArgumentException.class,()->Calculation.acos(1.00001));
    }
}