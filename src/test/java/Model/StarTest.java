package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StarTest {

    Star star;
    Location loc;

    @BeforeEach
    private void initStar(){
        loc = new Location(10,1,34);
        star = new Star(92,loc);
    }

    @Test
    void getShiningLevel() {
        assertEquals(92,star.getShiningLevel());
    }

    @Test
    void getLocation() {
        assertEquals(loc.getX(),star.getLocation().getX());
        assertEquals(loc.getY(),star.getLocation().getY());
        assertEquals(loc.getZ(),star.getLocation().getZ());
    }
}