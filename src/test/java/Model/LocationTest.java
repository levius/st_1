package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LocationTest {


    Location loc;
    Location secondLocation;

    @BeforeEach
    private void initLocation(){
        loc = new Location(10,10,10);
        secondLocation = new Location(2,4,10);
    }

    @Test
    void getX() {
        assertEquals(10,loc.getZ());
    }

    @Test
    void getY() {
        assertEquals(10,loc.getZ());
    }

    @Test
    void getZ() {
        assertEquals(10,loc.getZ());
    }

    @Test
    void getDistance() {
        assertEquals(10,loc.getDistance(secondLocation));
    }
}