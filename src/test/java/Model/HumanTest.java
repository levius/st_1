package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    Human human;
    Location loc;

    @BeforeEach
    private void initHuman(){
        loc = new Location(10,1,34);
        human = new Human("John",25,73,loc);
    }

    @Test
    void getLocation() {
        assertEquals(loc.getX(),human.getLocation().getX());
        assertEquals(loc.getY(),human.getLocation().getY());
        assertEquals(loc.getZ(),human.getLocation().getZ());
    }

    @Test
    void getFlying() {
     human.setFlying(true);
     assertEquals(true,human.getFlying());
    }

    @Test
    void setFlying() {
        human.setFlying(false);
        assertEquals(false,human.getFlying());
    }
}