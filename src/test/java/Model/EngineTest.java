package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EngineTest {

    Engine engine;
    @BeforeEach
    private void initEngine(){
        Location loc = new Location(10,1,34);
        engine = new Engine(true,10,32,loc);
    }

    @Test
    void isWorking() {
        assertEquals(true,engine.isWorking());
    }
}