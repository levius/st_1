package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class SpaceTest {


    Space space;
    Location locFirst;
    Location locSecond;
    Star firstStar;
    Star secondStar;
    Engine engine1;
    Engine engine2;
    Human human1;
    Human human2;


    @BeforeEach
    private void initHuman(){
        locFirst = new Location(0,0,0);
        locSecond = new Location(10,10,10);
        space = new Space();
        firstStar = new Star(100,locFirst);
        secondStar = new Star(80,locSecond);
        engine1 = new Engine(true, 220, 220, locFirst);
        engine2 = new Engine(true, 132, 150, locSecond);
        human1 = new Human("Григорий",34,74,new Location(2,2,2));
        human2 = new Human("Виктор",24,68,new Location(20,20,20));
    }
    @Test
    void getClosestStar() {
        space.addStar(firstStar);
        space.addStar(secondStar);
        assertEquals(firstStar,space.getClosestStar(new Location(1,1,1)));
        assertEquals(secondStar,space.getClosestStar(new Location(9,9,9)));
    }

    @Test
    void getClosestSoundSource() {
        space.addSoundSource(engine1);
        space.addSoundSource(engine2);
        assertEquals(engine1,space.getClosestSoundSource(new Location(1,1,1)));
        assertEquals(engine2,space.getClosestSoundSource(new Location(9,9,9)));
    }

    @Test
    void getNoiseLevel() {
        space.addSoundSource(engine1);
        assertEquals(220,space.getNoiseLevel(new Location(1,1,1)));
        assertEquals(0,space.getNoiseLevel(new Location(1000,1000,1000)));
    }

    @Test
    void getLightLevel() {
        assertEquals(0,space.getLightLevel(new Location(1,1,1)));
        space.addStar(firstStar);
        assertEquals(100,space.getLightLevel(new Location(1,1,1)));
    }

    @Test
    void humanWentFlying() {
        space.addSoundSource(engine1);
        space.addSoundSource(engine2);

        space.humanWentFlying(human1);
        assertEquals(true,human1.getFlying());

        space.humanWentFlying(human2);
        assertEquals(false, human2.getFlying());
    }
}