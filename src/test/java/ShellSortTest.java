import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import java.util.Arrays;


class ShellSortTest {
    ShellSort ShellSorter;
    @BeforeEach
    private void initSorter(){
        ShellSorter = new ShellSort();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/input.csv")
    void shellSort(String inputString, int swaps, int steps) {
        int[] inputArray = parseInput(inputString);
        int[] shellSortedArray = ShellSorter.shellSort(inputArray);
        Arrays.sort(inputArray);

        Assertions.assertEquals(inputArray,shellSortedArray);
        Assertions.assertEquals(swaps,ShellSorter.getSwapCounter());
        Assertions.assertEquals(steps, ShellSorter.getStepChangeCounter());
    }

    public int[] parseInput(String input) {
        String[] stringArray = input.split(" ");
        int [] result = new int [stringArray.length];
        for (int i =0; i < stringArray.length; i++) {
            result[i] = Integer.parseInt(stringArray[i]);
        }
        return result;
    }
}